%% plot comparison of uncertainty effects in HDDM parameters

% load overview structure

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data');
pn.figures = fullfile(pn.root, 'figures');
addpath(fullfile(pn.root, 'tools', 'cell2csv'));
addpath(fullfile(pn.root, 'tools'));
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));

load(fullfile(pn.root, '..', '..', 'stsw_multimodal', 'data', 'STSWD_summary_YAOA.mat'))

idx_YA = cellfun(@str2num, STSWD_summary.IDs)<2000;
idx_OA = cellfun(@str2num, STSWD_summary.IDs)>2000;

%% compare l1 HDDM parameter effects

HDDM = [];

tmp = squeeze(nanmean(cat(3,STSWD_summary.HDDM_vat_matched.driftEEG, STSWD_summary.HDDM_vat_matched.driftMRI),3));
tmp(tmp==0)=NaN;
HDDM{1,1} = tmp(idx_YA,:);
HDDM{2,1} = tmp(idx_OA,:);

%% plot using RCPs, no within-subject centering

colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.3 .1 .1]];
paramLabels = {'Drift rate'}; 

h = figure('units','normalized','position',[.1 .1 .15 .2]);
for indAge = 1%:2
    hold on;

    curData{1} = HDDM{1,1};
    curData{2} = HDDM{2,1};
    
	% read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:2
            data{i, j} = squeeze(curData{j}(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData{j}(:,i)-...
                nanmean(curData{j}(:,:),2)+...
                repmat(nanmean(nanmean(curData{j}(:,:),2),1),size(curData{j}(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(1:2,:);

    box off
    h_rc = rm_raincloud(data, cl,1);
    view([90 -90]);
    axis ij
    box(gca,'off')
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indAge})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)

figureName = 'd_drift_yaoa';
saveas(h, fullfile(pn.figures, figureName), 'png');
saveas(h, fullfile(pn.figures, figureName), 'epsc');

[h, p] = ttest2(HDDM{1,1}(:,1), HDDM{2,1}(:,1))

%% absolute change, no within-subject centering

% baseline correction
HDDM{1,1} = (tmp(idx_YA,:)-repmat(tmp(idx_YA,1),1,4));
HDDM{2,1} = (tmp(idx_OA,:)-repmat(tmp(idx_OA,1),1,4));

colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.3 .1 .1]];
paramLabels = {'Drift rate (abs. change)'}; 

h = figure('units','normalized','position',[.2 .2 .1 .2]);
for indAge = 1%:2
    hold on;

    curData{1} = squeeze(nanmean(HDDM{1,1}(:,2:end),2));
    curData{2} = squeeze(nanmean(HDDM{2,1}(:,2:end),2));
    
	% read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:1
        for j = 1:2
            data{i, j} = squeeze(curData{j}(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData{j}(:,i)-...
                nanmean(curData{j}(:,:),2)+...
                repmat(nanmean(nanmean(curData{j}(:,:),2),1),size(curData{j}(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(1:2,:);

    box off
    h_rc = rm_raincloud(data, cl,1);
    view([90 -90]);
    axis ij
    box(gca,'off')
    set(gca, 'YTickLabels', {'2-4'});
    ylabel('Target load'); xlabel(paramLabels{1})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    %curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(2)+.75*(curYTick(2)-curYTick(1))]);
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)

figureName = 'd_drift_changeabs_yaoa';
saveas(h, fullfile(pn.figures, figureName), 'png');
saveas(h, fullfile(pn.figures, figureName), 'epsc');

[h, p] = ttest2(curData{1}, curData{2})

%% percent change, no within-subject centering

% baseline correction
HDDM{1,1} = (tmp(idx_YA,:)-repmat(tmp(idx_YA,1),1,4))./repmat(tmp(idx_YA,1),1,4)*100;
HDDM{2,1} = (tmp(idx_OA,:)-repmat(tmp(idx_OA,1),1,4))./repmat(tmp(idx_OA,1),1,4)*100;

colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.3 .1 .1]];
paramLabels = {'Drift rate (% change)'}; 

h = figure('units','normalized','position',[.2 .2 .1 .2]);
for indAge = 1%:2
    hold on;

    curData{1} = squeeze(nanmean(HDDM{1,1}(:,2:end),2));
    curData{2} = squeeze(nanmean(HDDM{2,1}(:,2:end),2));
    
	% read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:1
        for j = 1:2
            data{i, j} = squeeze(curData{j}(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData{j}(:,i)-...
                nanmean(curData{j}(:,:),2)+...
                repmat(nanmean(nanmean(curData{j}(:,:),2),1),size(curData{j}(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(1:2,:);

    box off
    h_rc = rm_raincloud(data, cl,1);
    view([90 -90]);
    axis ij
    box(gca,'off')
    set(gca, 'YTickLabels', {'2-4'});
    ylabel('Target load'); xlabel(paramLabels{1})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    %curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(2)+.75*(curYTick(2)-curYTick(1))]);
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)

figureName = 'd_drift_changerel_yaoa';
saveas(h, fullfile(pn.figures, figureName), 'png');
saveas(h, fullfile(pn.figures, figureName), 'epsc');

[h, p] = ttest2(curData{1}, curData{2})

%% create subgroups within YA and OA based on L1 drift

[~, sortIndYA] = sort(tmp(idx_YA,1), 'descend');
[~, sortIndOA] = sort(tmp(idx_OA,1), 'descend');

N_ya = numel(sortIndYA);
N_oa = numel(sortIndOA);

idx_YA = find(idx_YA);
idx_OA = find(idx_OA);

% baseline correction
HDDM{1,1} = (tmp(idx_YA(sortIndYA(1:floor(N_ya/2))),:)-...
    repmat(tmp(idx_YA(sortIndYA(1:floor(N_ya/2))),1),1,4))./...
    repmat(tmp(idx_YA(sortIndYA(1:floor(N_ya/2))),1),1,4);
HDDM{2,1} = (tmp(idx_YA(sortIndYA(floor(N_ya/2)+1:end)),:)-...
    repmat(tmp(idx_YA(sortIndYA(floor(N_ya/2)+1:end)),1),1,4))./...
    repmat(tmp(idx_YA(sortIndYA(floor(N_ya/2)+1:end)),1),1,4);
HDDM{1,2} = (tmp(idx_OA(sortIndOA(1:floor(N_oa/2))),:)-...
    repmat(tmp(idx_OA(sortIndOA(1:floor(N_oa/2))),1),1,4))./...
    repmat(tmp(idx_OA(sortIndOA(1:floor(N_oa/2))),1),1,4);
HDDM{2,2} = (tmp(idx_OA(sortIndOA((floor(N_oa/2)+1:end))),:)-...
    repmat(tmp(idx_OA(sortIndOA((floor(N_oa/2)+1:end))),1),1,4))./...
    repmat(tmp(idx_OA(sortIndOA((floor(N_oa/2)+1:end))),1),1,4);

colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.1 .3 .1]; 2.*[.1 .1 .3]];
paramLabels = {'Drift rate (% change)'}; 
groups = {'YA'; 'OA'};

h = figure('units','normalized','position',[.1 .1 .2 .2]);
for indAge = 1:2
    subplot(1,2,indAge);
    hold on;

    curData{1} = squeeze(nanmean(HDDM{1,indAge}(:,2:end),2));
    curData{2} = squeeze(nanmean(HDDM{2,indAge}(:,2:end),2));
    
	% read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:1
        for j = 1:2
            data{i, j} = squeeze(curData{j}(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData{j}(:,i)-...
                nanmean(curData{j}(:,:),2)+...
                repmat(nanmean(nanmean(curData{j}(:,:),2),1),size(curData{j}(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(1:2,:);

    box off
    h_rc = rm_raincloud(data, cl,1);
    view([90 -90]);
    axis ij
    box(gca,'off')
    set(gca, 'YTickLabels', {'2-4'});
    ylabel('Target load'); 
    if indAge == 1
        xlabel(paramLabels{1})
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    title(groups{indAge});
    %curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(2)+.75*(curYTick(2)-curYTick(1))]);
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)

%% correlations between absolute and relative change?

HDDM{1,1} = tmp(idx_YA,:);
HDDM{2,1} = tmp(idx_OA,:);

HDDM_pc{1,1} = (tmp(idx_YA,:)-repmat(tmp(idx_YA,1),1,4))./repmat(tmp(idx_YA,1),1,4);
HDDM_pc{2,1} = (tmp(idx_OA,:)-repmat(tmp(idx_OA,1),1,4))./repmat(tmp(idx_OA,1),1,4);


figure; 
hold on;
scatter(squeeze(nanmean(HDDM{1,1}(:,2:4),2))-HDDM{1,1}(:,1), ...
    squeeze(nanmean(HDDM_pc{1,1}(:,2:4),2)), 'r', 'filled')
scatter(squeeze(nanmean(HDDM{2,1}(:,2:4),2))-HDDM{2,1}(:,1), ...
    squeeze(nanmean(HDDM_pc{2,1}(:,2:4),2)), 'k', 'filled')
xlabel('absolute change 2-4 vs 1')
ylabel('percent change 2-4 vs 1')
legend({'YA', 'OA'})
set(findall(gcf,'-property','FontSize'),'FontSize',22)

% figure; 
% hold on;
% scatter(squeeze(nanmean(HDDM{1,1}(:,2:4),2)), ...
%     squeeze(nanmean(HDDM_pc{1,1}(:,2:4),2)), 'r', 'filled')
% scatter(squeeze(nanmean(HDDM{2,1}(:,2:4),2)), ...
%     squeeze(nanmean(HDDM_pc{2,1}(:,2:4),2)), 'k', 'filled')
% xlabel('absolute change 2-4 vs 1')
% ylabel('relative change 2-4 vs 1')
% legend({'YA', 'OA'})
% set(findall(gcf,'-property','FontSize'),'FontSize',22)

%figure; scatter(HDDM{2,1}(:,1), squeeze(nanmean(HDDM_pc{2,1}(:,2:4),2)))

%% quantify as absoulte and relative linear change (= main analysis)

%% absolute change, no within-subject centering

% baseline correction
HDDM{1,1} = (tmp(idx_YA,:)-repmat(tmp(idx_YA,1),1,4));
HDDM{2,1} = (tmp(idx_OA,:)-repmat(tmp(idx_OA,1),1,4));

% calculate linear changes
X = [1 1; 1 2; 1 3; 1 4];
b=X\HDDM{1,1}'; HDDMlinear{1,1}(:,1) = b(2,:);
b=X\HDDM{2,1}'; HDDMlinear{2,1}(:,1) = b(2,:);

colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.3 .1 .1]];
paramLabels = {'Linear mod. (drift)'}; 

h = figure('units','normalized','position',[.2 .2 .1 .2]);
set(gcf,'renderer','Painters')
hold on;

curData{1} = squeeze(nanmean(HDDMlinear{1,1}(:,1),2));
curData{2} = squeeze(nanmean(HDDMlinear{2,1}(:,1),2));

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:1
    for j = 1:2
        data{i, j} = squeeze(curData{j}(:,i));
    end
end

cl = colorm(1:2,:);

box off
h_rc = rm_raincloud(data, cl,1);
view([90 -90]);
axis ij
box(gca,'off')
set(gca, 'YTickLabels', {'absolute'});
xlabel(paramLabels{1})
set(findall(gcf,'-property','FontSize'),'FontSize',20)
[h, p] = ttest2(curData{1}, curData{2});
title(['p = ', sprintf('%.2f', p)]);

set(findall(gcf,'-property','FontSize'),'FontSize',22)

figureName = 'd_drift_changeabs_linear_yaoa';
saveas(h, fullfile(pn.figures, figureName), 'png');
saveas(h, fullfile(pn.figures, figureName), 'epsc');

[h, p] = ttest2(curData{1}, curData{2})

%% percent change, no within-subject centering
% This needs additional outlier exclusion

% baseline correction
HDDM{1,1} = (tmp(idx_YA,:)-repmat(tmp(idx_YA,1),1,4))./repmat(tmp(idx_YA,1),1,4)*100;
HDDM{2,1} = (tmp(idx_OA,:)-repmat(tmp(idx_OA,1),1,4))./repmat(tmp(idx_OA,1),1,4)*100;

% calculate linear changes
X = [1 1; 1 2; 1 3; 1 4];
b=X\HDDM{1,1}'; HDDMlinear{1,1}(:,1) = b(2,:);
b=X\HDDM{2,1}'; HDDMlinear{2,1}(:,1) = b(2,:);

% winsorize outlier
HDDMlinear{1,1}(:,1) = winsorize_outlier(HDDMlinear{1,1}(:,1));
HDDMlinear{2,1}(:,1) = winsorize_outlier(HDDMlinear{2,1}(:,1));

colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.3 .1 .1]];
paramLabels = {'Linear mod. (drift)'}; 

h = figure('units','normalized','position',[.2 .2 .1 .2]);
set(gcf,'renderer','Painters')
hold on;

curData{1} = squeeze(nanmean(HDDMlinear{1,1}(:,1),2));
curData{2} = squeeze(nanmean(HDDMlinear{2,1}(:,1),2));

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:1
    for j = 1:2
        data{i, j} = squeeze(curData{j}(:,i));
    end
end

cl = colorm(1:2,:);

box off
h_rc = rm_raincloud(data, cl,1);
view([90 -90]);
axis ij
box(gca,'off')
xlim([-35, 0])
set(gca, 'YTickLabels', {'% change'});
xlabel(paramLabels{1})
[h, p] = ttest2(curData{1}, curData{2});
title(['p = ', sprintf('%.2f', p)]);

set(findall(gcf,'-property','FontSize'),'FontSize',22)

figureName = 'd_drift_changerel_linear_yaoa';
saveas(h, fullfile(pn.figures, figureName), 'png');
saveas(h, fullfile(pn.figures, figureName), 'epsc');

close(h)

[h, p] = ttest2(curData{1}, curData{2})