% bring data into HDDM format:
% Columns: condition (seperate subjects here), correct/incorrect, RTs (in s) 

% create input data for combined YA & OA, EEG & MRI analyses

clear all; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.dataIn = fullfile(pn.root, '..', 'behavior','data');
pn.dataOut = fullfile(pn.root, 'data');

addpath(fullfile(pn.root, 'tools', 'cell2csv'));

load(fullfile(pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'),...
    'MergedDataEEG', 'MergedDataMRI');

load(fullfile(pn.root, '..', 'behavior', 'data', 'SummaryData_N102.mat'), 'SummaryData', 'IDs_all');

modalities = {'EEG'; 'MRI'};

for indModality = 1:numel(modalities)

    %% get vectors of RTs and Acc

    RTs_vector = reshape(eval(['MergedData', modalities{indModality}, '.RTs']),1,[]);
    Acc_vector = reshape(eval(['MergedData', modalities{indModality}, '.Accs']),1,[]);

    %% create vector of IDs

    Sub_vector = reshape(repmat(1:102,256,1),1,[]);

    %% create vector of age

    ageVec = [];
    IDs = cell2mat(IDs_all);
    IDs = str2mat(IDs(:,1));
    for indEntry = 1:numel(IDs)
        if strcmp(IDs(indEntry), '1')
            %ageVec{1,indEntry} = 'YA';
            ageVec(1,indEntry) = 1;
        elseif strcmp(IDs(indEntry), '2')
            %ageVec{1,indEntry} = 'OA';
            ageVec(1,indEntry) = 2;
        end
    end; clear IDs;

    Age_vector = reshape(repmat(ageVec,256,1),1,[]);

    %% create vector of dimensionality

    Dim_vector = reshape(eval(['MergedData', modalities{indModality}, '.StateOrders']),1,[]);

    %% create vector of attribute

    Att_vector = reshape(eval(['MergedData', modalities{indModality}, '.Atts']),1,[]);

    %% combine in data matrix

    data_tmp{indModality} = [];
    data_tmp{indModality} = [Sub_vector', Acc_vector', RTs_vector', Dim_vector', Att_vector', Age_vector'];

    %% set any RTs below 250 ms to NaN

    data_tmp{indModality}(find(RTs_vector<=.25),:) = NaN;

    %% delete any rows with nans

    data_tmp{indModality}(any(isnan(data_tmp{indModality}), 2), :) = [];

    %% add complete ID

    IDs_mat =  cellfun(@str2num,IDs_all);
    data_tmp{indModality}(:,7) = IDs_mat(data_tmp{indModality}(:,1)); clear IDs_mat;
    
    %% add modality
    
    data_tmp{indModality}(:,8) = indModality;

end

data = cat(1, data_tmp{1}, data_tmp{2});

%% add headers

data = num2cell(data);
data = [{'subject'},{'acc'},{'rt'},{'dim'},{'att'},{'age'},{'ID'},{'session'};data];

data([data{:,8}]==1,8) = modalities(1);
data([data{:,8}]==2,8) = modalities(2);

%% export data matrix

save(fullfile(pn.dataOut, 'StateSwitchDynamicTrialData.mat'),'data')

%% export data matrix as .csv

cell2csv(fullfile(pn.dataOut, 'StateSwitchDynamicTrialData.dat'),data)