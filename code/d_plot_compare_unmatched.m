%% plot comparison of uncertainty effects in HDDM parameters

% load overview structure

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data');
pn.plotFolder = fullfile(pn.root, 'figures');
addpath(fullfile(pn.root, 'tools', 'cell2csv'));
addpath(fullfile(pn.root, 'tools'));

load(fullfile(pn.root, '..', '..', 'stsw_multimodal', 'data', 'STSWD_summary_YAOA.mat'))

idx_YA = cellfun(@str2num, STSWD_summary.IDs)<2000;
idx_OA = cellfun(@str2num, STSWD_summary.IDs)>2000;

%% compare l1 HDDM parameter effects

HDDM = [];

tmp = squeeze(nanmean(cat(3,STSWD_summary.HDDM_vat_matched.driftEEG-STSWD_summary.HDDM_vat.driftEEG, ...
    STSWD_summary.HDDM_vat_matched.driftMRI-STSWD_summary.HDDM_vat.driftMRI),3));
tmp(tmp==0)=NaN;
HDDM{1,1} = tmp(idx_YA,:);
HDDM{2,1} = tmp(idx_OA,:);

%% plot using RCPs, no within-subject centering

colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.3 .1 .1]];
paramLabels = {'Drift rate ch.'}; 

h = figure('units','normalized','position',[.1 .1 .2 .2]);
for indAge = 1%:2
    hold on;

    curData{1} = HDDM{1,1};
    curData{2} = HDDM{2,1};
    
	% read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:2
            data{i, j} = squeeze(curData{j}(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData{j}(:,i)-...
                nanmean(curData{j}(:,:),2)+...
                repmat(nanmean(nanmean(curData{j}(:,:),2),1),size(curData{j}(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(1:2,:);

    box off
    h_rc = rm_raincloud(data, cl,1);
    view([90 -90]);
    axis ij
    box(gca,'off')
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indAge})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
end
title('Change with matching')
set(findall(gcf,'-property','FontSize'),'FontSize',22)

[h, p] = ttest(curData{1}(:,1), curData{1}(:,4))
[h, p] = ttest(curData{2}(:,1), curData{2}(:,4))
