% plot HDDM parameter estimates

savefig = 1;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.data = fullfile(pn.root, 'data');
pn.plotFolder = fullfile(pn.root, 'figures');
addpath(genpath(fullfile(pn.root, 'tools', 'RainCloudPlots')));
addpath(fullfile(pn.root, 'tools'));

% load data as table, convert to array
dataFile = fullfile(pn.data, 'hddm_all', 'data.csv');
tableData = readtable(dataFile, 'ReadRowNames', 1);
arrayData = table2array(tableData);
ColumnNames = tableData.Properties.VariableNames'; clear tableData;

% get individual subject data

params = {'v'; 'a'; 't'};
ageGroups = {'1', '2'};
conditions_load = {'1'; '2'; '3'; '4'};
conditions_feature = {'1'; '2'; '3'; '4'};

% Note: here we have estimates for more participants, as we jointly fit
% both EEG and MRI sessions (i.e., even if an MRI session was missing).
% N = 49 YA, N = 53 OA

Indices = []; EntryNum = []; MeanValues = [];
MeanValues = NaN(numel(params), numel(conditions_load), numel(conditions_feature), 102);
for indParam = 1:numel(params)
    for indAge = 1:numel(ageGroups)
        for indCond_l = 1:numel(conditions_load)
            for indCond_f = 1:numel(conditions_feature)
                strPattern=[params{indParam}, '_subj_',...
                    ageGroups{indAge}, '_0_',...
                    conditions_load{indCond_l},'_0_',...
                    conditions_feature{indCond_f},'_0_'];
                Indices = find(contains(ColumnNames,strPattern));
                EntryNum{indAge,indParam,indCond_l,indCond_f} = ...
                    strrep(cellfun(@(x) x(end-4:end-1),ColumnNames(Indices),...
                    'un',false), '_', '');
                EntryNum{indAge,indParam,indCond_l,indCond_f} = ...
                    cell2mat(cellfun(@str2num,EntryNum{indAge,indParam,indCond_l,indCond_f},'un',0));
                MeanValues(indParam,indCond_l,indCond_f,EntryNum{indAge,indParam,indCond_l,indCond_f}) = ...
                    nanmean(arrayData(:,Indices),1);
            end
        end
    end
end
clear EntryNum Indices

% MeanValues - parameter x load x feature x ID

% subjects are indexed with respect to IDs_all
pn.dataIn = fullfile(pn.root, '..', 'behavior', 'data');
load(fullfile(pn.dataIn, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'),'IDs_all');

filename = fullfile(pn.root, 'code', 'id_list_eegmr.txt');
fileID = fopen(filename);
IDs_EEGMR = textscan(fileID,'%s');
fclose(fileID);
IDs_EEGMR = IDs_EEGMR{1};

% select subjects (multimodal only)
idxMulti_summary = ismember(IDs_all, IDs_EEGMR);
[IDs_all(idxMulti_summary), IDs_EEGMR]

idx_YA = cellfun(@str2num, IDs_all)<2000;
idx_OA = cellfun(@str2num, IDs_all)>2000;

% handle data with different ID lists

% STSWD_summary.IDs - N = 109
% IDs_all - N = 102
% IDs_EEGMR - N = 95 | 42 YA, 53 OA

%% set non-multimodal data to NaN

MeanValues(:,:,:,idxMulti_summary==0) = NaN;

%% Optional: plot feature-specific single-target drift rates using RCPs

colorm = [0.0314, 0.3176, 0.6118; 2.*[.3 .1 .1]; 2.*[.3 .1 .1]];
paramLabels = {'Drift rate'; 'Threshold'; 'Non-decision time'}; 

features = {'color', 'motion', 'size', 'luminance'};

for indFeature = 1:4
h = figure('units','normalized','position',[.1 .1 .15 .6]);
set(gcf,'renderer','Painters')
for indIdx = 1:3
    subplot(3,1,indIdx)
    cla; hold on;

    curData = [];
    curData{1} = squeeze(permute(squeeze(MeanValues(indIdx,indFeature,:,idx_YA)),[3,1,2]))';
    curData{2} = squeeze(permute(squeeze(MeanValues(indIdx,indFeature,:,idx_OA)),[3,1,2]))';
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:2
            data{i, j} = squeeze(curData{j}(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData{j}(:,i)-...
                nanmean(curData{j}(:,:),2)+...
                repmat(nanmean(nanmean(curData{j}(:,:),2),1),size(curData{j}(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm([1,2],:);

    box off
    cla;
        h_rc = rm_raincloud(data, cl,1);
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    %xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
    suptitle(features{indFeature})
end
end

%% plot single-target estimates by feature and age

features = {'color', 'motion', 'size', 'luminance'};

h = figure('units','normalized','position',[.1 .1 .15 .6]);
set(gcf,'renderer','Painters')
for indIdx = 1:3
    subplot(3,1,indIdx)
    cla; hold on;

    curData = [];
    curData{1} = squeeze(permute(squeeze(nanmean(MeanValues(indIdx,:,1,idx_YA),3)),[3,1,2]))';
    curData{2} = squeeze(permute(squeeze(nanmean(MeanValues(indIdx,:,1,idx_OA),3)),[3,1,2]))';
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:2
            data{i, j} = squeeze(curData{j}(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData{j}(:,i)-...
                nanmean(curData{j}(:,:),2)+...
                repmat(nanmean(nanmean(curData{j}(:,:),2),1),size(curData{j}(:,:),1),1);
        end
    end

    cl = colorm([1,2],:);

    box off
    cla;
        h_rc = rm_raincloud(data, cl,1);
        view([90 -90]);
        axis ij
    box(gca,'off')
    ylabel('features'); 
    xlabel(paramLabels{indIdx})
    set(gca, 'YTickLabels', fliplr(features),'FontSize',12)
    %xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
end
suptitle("single-target")
set(findall(gcf,'-property','FontSize'),'FontSize',12)

if savefig == 1
    figureName = 'c1_singletarget_byfeature';
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
end

%% plot single-feature drift by feature (older-younger)

% h = figure('units','normalized','position',[.1 .1 .15 .6]);
% for indIdx = 1
%     cla; hold on;
% 
%     curData = [];
%     curData{1} = squeeze(permute(squeeze(nanmean(MeanValues(indIdx,:,1,idx_YA),3)),[3,1,2]))';
%     curData{2} = squeeze(permute(squeeze(nanmean(MeanValues(indIdx,:,1,idx_OA),3)),[3,1,2]))';
%     
%  % read into cell array of the appropriate dimensions
%     data = []; data_ws = [];
%     for i = 1:4
%         for j = 1:2
%             data{i, j} = squeeze(curData{j}(:,i));
%         end
%     end
% 
%     cl = colorm([1,2],:);
% 
%     box off
%     cla;
%         h_rc = rm_raincloud(data, cl,1);
%         view([90 -90]);
%         axis ij
%     box(gca,'off')
%     ylabel('features'); 
%     xlabel(paramLabels{indIdx})
%     set(gca, 'YTickLabels', fliplr(features),'FontSize',12)
%     %xlim(lims{indIdx}); 
%     curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
% end
% suptitle("single-target")
% set(findall(gcf,'-property','FontSize'),'FontSize',12)

%% propensity score matching
% 
% curData{1} = squeeze(MeanValues(1,:,:,idx_YA));
% curData{2} = squeeze(MeanValues(1,:,:,idx_OA));
% 
% for indAtt = 1:4
%     dat_ya = squeeze(curData{1}(indAtt,:,:))';
%     dat_oa = squeeze(curData{2}(indAtt,:,:))';
%     
%     supportrange = .05;
% 
%     [matchings] = matchpairs(abs(dat_ya(:,1)-dat_oa(:,1)'), supportrange, 'min');
%     
%     row = unique(matchings(:,1));
%     column = unique(matchings(:,2));
%     
%     mappings{indAtt,1} = row;
%     mappings{indAtt,2} = column;
% 
%     data_ya_matched(indAtt, :,:) = dat_ya; data_ya_matched(indAtt, :,:) = NaN;
%     data_ya_matched(indAtt, row,:) = dat_ya(row,:);
%     data_ya_unmatched(indAtt, :,:) = dat_ya; data_ya_unmatched(indAtt, :,:) = NaN;
%     data_ya_unmatched(indAtt, setdiff(1:end,row),:) = dat_ya(setdiff(1:end,row),:);
%     
%     data_oa_matched(indAtt, :,:) = dat_oa; data_oa_matched(indAtt, :,:) = NaN;
%     data_oa_matched(indAtt, column,:) = dat_oa(column,:);
%     data_oa_unmatched(indAtt, :,:) = dat_oa; data_oa_unmatched(indAtt, :,:) = NaN;
%     data_oa_unmatched(indAtt, setdiff(1:end,column),:) = dat_oa(setdiff(1:end,column),:);
% end

%% plot matched and unmatched data

% figure;
%     hold on;
%     error = squeeze(nanstd(nanmean(data_ya_matched(1:4,:,:),1),[],2))./sqrt(size(data_ya_matched,2));
%     errorbar(squeeze(nanmean(nanmean(data_ya_matched(1:4,:,:),1),2)), error, 'k', ...
%         'linewidth', 2);
%     error = squeeze(nanstd(nanmean(data_oa_matched(1:4,:,:),1),[],2))./sqrt(size(data_oa_matched,2));
%     errorbar(squeeze(nanmean(nanmean(data_oa_matched(1:4,:,:),1),2)), error, 'r', ...
%         'linewidth', 2);
%     error = squeeze(nanstd(nanmean(data_ya_unmatched(1:4,:,:),1),[],2))./sqrt(size(data_ya_matched,2));
%     errorbar(squeeze(nanmean(nanmean(data_ya_unmatched(1:4,:,:),1),2)), error, 'k--', ...
%         'linewidth', 2);
%     error = squeeze(nanstd(nanmean(data_oa_unmatched(1:4,:,:),1),[],2))./sqrt(size(data_oa_matched,2));
%     errorbar(squeeze(nanmean(nanmean(data_oa_unmatched(1:4,:,:),1),2)), error, 'r--', ...
%         'linewidth', 2);
%     set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
%             'xlim', [0.5 4.5]);
%     ylabel('Drift rate'); xlabel('# of targets');
%     set(findall(gcf,'-property','FontSize'),'FontSize',18);
%     legend({'YA'; 'OA'; 'YA unmatch'; 'OA unmatch'});
% 
% [h, p] = ttest2(nanmean(data_ya_matched(:,:,1),1), nanmean(data_oa_matched(:,:,1),1))
% [h, p] = ttest2(nanmean(data_ya_matched(:,:,4),1), nanmean(data_oa_matched(:,:,4),1))

%% create mean values structure that is sorted by best to worst feature w.r.t. L1

MeanValues_cens = MeanValues;
%MeanValues_cens(:,2,:,:) = NaN;
for indIdx = 1:3
    for indID = 1:size(MeanValues,4)
        [sortVals(indID,:), sortIdx(indID,:)] = sort(squeeze(MeanValues(indIdx,:,1,indID)), 'descend');
        MeanValues_sorted(indIdx,:,:,indID) = squeeze(MeanValues_cens(indIdx,sortIdx(indID,:),:,indID));
    end
end

% plot results for approx. matched features

% h = figure('units','normalized','position',[.1 .1 .15 .2]);
% indIdx = 1;
% cla; hold on;
% 
% curData = [];
% curData{1} = squeeze(permute(squeeze(nanmean(MeanValues_sorted(indIdx,[2],:,idx_YA),2)),[3,1,2]))';
% curData{2} = squeeze(permute(squeeze(nanmean(MeanValues_sorted(indIdx,[1],:,idx_OA),2)),[3,1,2]))';
% 
% % read into cell array of the appropriate dimensions
% data = [];
% for i = 1:4
%     for j = 1:2
%         data{i, j} = squeeze(curData{j}(:,i));
%     end
% end
% 
% cl = colorm([1,2],:);
% 
% box off
% cla;
%     h_rc = rm_raincloud(data, cl,1);
%     view([90 -90]);
%     axis ij
% box(gca,'off')
% %set(gca, 'YTick', [1,2,3,4]);
% set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
% ylabel('Target load'); xlabel(paramLabels{indIdx})
% set(findall(gcf,'-property','FontSize'),'FontSize',20)
% % xlim([-1 4]); 
% curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
% title("single-target matched age")

%% plot drift rate for sorted feature drift at L1 (in order of preference)

features = {'best', '2nd', '3rd', 'worst'};

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
cla; hold on;

curData = [];
curData{1} = squeeze(permute(squeeze(nanmean(MeanValues_sorted(1,:,1,idx_YA),3)),[3,1,2]))';
curData{2} = squeeze(permute(squeeze(nanmean(MeanValues_sorted(1,:,1,idx_OA),3)),[3,1,2]))';

% read into cell array of the appropriate dimensions
data = []; 
for i = 1:4
    for j = 1:2
        data{i, j} = squeeze(curData{j}(:,i));
    end
end

cl = colorm([1,2],:);

box off
cla;
    h_rc = rm_raincloud(data, cl,1);
    view([90 -90]);
    axis ij
box(gca,'off')
ylabel('features'); 
xlabel(paramLabels{1})
set(gca, 'YTickLabels', fliplr(features),'FontSize',12)
%xlim(lims{indIdx}); 
curYTick = get(gca, 'YTick'); ylim([curYTick(1)-0.5*(curYTick(2)-curYTick(1)) curYTick(4)+1*(curYTick(2)-curYTick(1))]);
%suptitle("single-target")
set(findall(gcf,'-property','FontSize'),'FontSize',20)

if savefig == 1
    figureName = 'c1_driftsorting';
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
end

%% plot for remaining parameters

indIdx = 3;

features = {'best', '2nd', '3rd', 'worst'};

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
cla; hold on;

curData = [];
curData{1} = squeeze(permute(squeeze(nanmean(MeanValues_sorted(indIdx,:,1,idx_YA),3)),[3,1,2]))';
curData{2} = squeeze(permute(squeeze(nanmean(MeanValues_sorted(indIdx,:,1,idx_OA),3)),[3,1,2]))';

% read into cell array of the appropriate dimensions
data = []; 
for i = 1:4
    for j = 1:2
        data{i, j} = squeeze(curData{j}(:,i));
    end
end

cl = colorm([1,2],:);

box off
cla;
    h_rc = rm_raincloud(data, cl,1);
    view([90 -90]);
    axis ij
box(gca,'off')
ylabel('features'); 
xlabel(paramLabels{indIdx})
set(gca, 'YTickLabels', fliplr(features),'FontSize',12)
%xlim(lims{indIdx}); 
curYTick = get(gca, 'YTick'); ylim([curYTick(1)-0.5*(curYTick(2)-curYTick(1)) curYTick(4)+1*(curYTick(2)-curYTick(1))]);
%suptitle("single-target")
set(findall(gcf,'-property','FontSize'),'FontSize',20)

%% plot comparable visualization to accuracy

indIdx = 1;

h = figure('units','normalized','position',[.1 .1 .15 .25]);
set(gcf,'renderer','Painters')
    hold on;
    curData = squeeze(permute(squeeze(nanmean(MeanValues_sorted(indIdx,[1],:,idx_YA),2)),[3,1,2]))';
    error = squeeze(nanstd(curData,[],1))./sqrt(size(curData,1));
    errorbar(squeeze(nanmean(curData,1)), error, '--k', ...
        'linewidth', 2);
    
    curData = squeeze(permute(squeeze(nanmean(MeanValues_sorted(indIdx,[2:4],:,idx_YA),2)),[3,1,2]))';
    error = squeeze(nanstd(curData,[],1))./sqrt(size(curData,1));
    errorbar(squeeze(nanmean(curData,1)), error, 'k', ...
        'linewidth', 2);
    
    curData = squeeze(permute(squeeze(nanmean(MeanValues_sorted(indIdx,[1],:,idx_OA),2)),[3,1,2]))';
    error = squeeze(nanstd(curData,[],1))./sqrt(size(curData,1));
    errorbar(squeeze(nanmean(curData,1)), error, 'r', ...
        'linewidth', 2);
    
    curData = squeeze(permute(squeeze(nanmean(MeanValues_sorted(indIdx,[2:4],:,idx_OA),2)),[3,1,2]))';
    error = squeeze(nanstd(curData,[],1))./sqrt(size(curData,1));
    errorbar(squeeze(nanmean(curData,1)), error, '--r', ...
        'linewidth', 2);
    
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Drift'); xlabel('# of targets');
    %title("Matched features only")
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
if savefig == 1
    figureName = 'c1_matcheddriftL1';
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
end

[h, p] = ttest2(squeeze(nanmean(MeanValues_sorted(1,2:4,1,idx_YA),2)), squeeze(nanmean(MeanValues_sorted(1,1,1,idx_OA),2)))
[h, p] = ttest2(squeeze(nanmean(MeanValues_sorted(1,2:4,2,idx_YA),2)), squeeze(nanmean(MeanValues_sorted(1,1,2,idx_OA),2)))
[h, p] = ttest2(squeeze(nanmean(MeanValues_sorted(1,2:4,3,idx_YA),2)), squeeze(nanmean(MeanValues_sorted(1,1,3,idx_OA),2)))
[h, p] = ttest2(squeeze(nanmean(MeanValues_sorted(1,2:4,4,idx_YA),2)), squeeze(nanmean(MeanValues_sorted(1,1,4,idx_OA),2)))

%% plot minimal comparison

h = figure('units','normalized','position',[.1 .1 .15 .25]);
set(gcf,'renderer','Painters')
    hold on;
    curData = squeeze(permute(squeeze(nanmean(MeanValues_sorted(indIdx,[2:4],:,idx_YA),2)),[3,1,2]))';
    error = squeeze(nanstd(curData,[],1))./sqrt(size(curData,1));
    errorbar(squeeze(nanmean(curData,1)), error, 'k', ...
        'linewidth', 2);
    
    curData = squeeze(permute(squeeze(nanmean(MeanValues_sorted(indIdx,[1],:,idx_OA),2)),[3,1,2]))';
    error = squeeze(nanstd(curData,[],1))./sqrt(size(curData,1));
    errorbar(squeeze(nanmean(curData,1)), error, 'r', ...
        'linewidth', 2);
    
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylabel('Mean Accuracy'); xlabel('# of targets');
    %title("Matched features only")
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
%%  quantify linear changes for matched data
    
X = [1 1; 1 2; 1 3; 1 4]; 
curData = squeeze(permute(squeeze(nanmean(MeanValues_sorted(indIdx,[2:4],:,idx_YA),2)),[3,1,2]))';
b=X\curData'; 
ya_matched_lin(:,1) = winsorize_outlier(b(2,:));
ya_matched_lin(isnan(ya_matched_lin)) = [];

curData = squeeze(permute(squeeze(nanmean(MeanValues_sorted(indIdx,[1],:,idx_OA),2)),[3,1,2]))';
b=X\curData';
oa_matched_lin(:,1) = winsorize_outlier(b(2,:));
oa_matched_lin(isnan(oa_matched_lin)) = [];

colorm = [.8 .8 .8; 8 .6 .6; .6 .8 1];

h = figure('units','centimeters','position',[.1 .1 7 8]);
set(gcf,'renderer','Painters')
plot_data{1} = ya_matched_lin;
plot_data{2} = oa_matched_lin;
set(gcf,'renderer','Painters')
cla;
hold on;
for indGroup = 1:2
    if ttest(plot_data{indGroup})==1 & nanmean(plot_data{indGroup})>0
        bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(2,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
    elseif ttest(plot_data{indGroup})==1 & nanmean(plot_data{indGroup})<0
        bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(3,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
    elseif ttest(plot_data{indGroup})==0
        bar(indGroup, nanmean(plot_data{indGroup}), 'FaceColor', colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.8);
    end
    % plot individual values on top
    scatter(repmat(indGroup,numel(plot_data{indGroup}),1)+(rand(numel(plot_data{indGroup}),1)-.5).*.4,...
        plot_data{indGroup}, 20, 'filled', 'MarkerFaceColor', [.3 .3 .3]);
end
xlim([.25 2.75]); %ylim([1.3 2])
set(gca,'xtick',[1,2]); set(gca,'xTickLabel',{'YA'; 'OA'}); 
%xlabel('Age Group'); 
ylabel(['linear change'])
set(findall(gcf,'-property','FontSize'),'FontSize',20)
[htest, p] = ttest2(plot_data{1}, plot_data{2});
if p>10^-3
    title(['p = ', sprintf('%.2f',p)]);
else
    tit = sprintf('p = %.1e',p);
    title(tit);
end

if savefig == 1
    figureName = 'c1_matcheddrift_linchange';
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
end

%% plot median split for LV1, apply to drift rates in a feature-specific fashion (L1 only)

pn.summary = fullfile(pn.root, '..', '..', 'stsw_multimodal', 'data');
load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'));

% remove IDs from summary structure that are not in the current data
idxMulti_summary_mismatch = 1-ismember(STSWD_summary.IDs, IDs_all);
STSWD_summary.SPM_lv1.data(find(idxMulti_summary_mismatch),:) = [];

[sortValYA, sortIdxYA] = sort(squeeze(nanmean(STSWD_summary.SPM_lv1.data(idxMulti_summary & idx_YA,2:4),2))...
    -STSWD_summary.SPM_lv1.data(idxMulti_summary & idx_YA,1), 'descend'); % here it is a negative slope
[sortValOA, sortIdxOA] = sort(squeeze(nanmean(STSWD_summary.SPM_lv1.data(idxMulti_summary & idx_OA,2:4),2))...
    -STSWD_summary.SPM_lv1.data(idxMulti_summary & idx_OA,1), 'descend'); % here it is a negative slope

idx_ya = find(idxMulti_summary & idx_YA);
lowChIdxYA = sortIdxYA(1:floor(numel(sortIdxYA)/3));
midChIdxYA = sortIdxYA(floor(numel(sortIdxYA)/3)+1:2*floor(numel(sortIdxYA)/3));
highChIdxYA = sortIdxYA(2*floor(numel(sortIdxYA)/3)+1:end);

idx_oa = find(idxMulti_summary & idx_OA);
lowChIdxOA = sortIdxOA(1:floor(numel(sortIdxOA)/3));
midChIdxOA = sortIdxOA(floor(numel(sortIdxOA)/3)+1:2*floor(numel(sortIdxOA)/3));
highChIdxOA = sortIdxOA(2*floor(numel(sortIdxOA)/3)+1:end);

% idx_ya = find(idxMulti_summary & idx_YA);
% lowChIdxYA = sortIdxYA(1:floor(numel(sortIdxYA)/2));
% highChIdxYA = sortIdxYA(floor(numel(sortIdxYA)/2)+1:end);
% 
% idx_oa = find(idxMulti_summary & idx_OA);
% lowChIdxOA = sortIdxOA(1:floor(numel(sortIdxOA)/2));
% highChIdxOA = sortIdxOA(floor(numel(sortIdxOA)/2)+1:end);

idxDrift = 1;

STSWD_summary.data_feature = squeeze(MeanValues(idxDrift,1:4,1,:))';
STSWD_summary.data_matched = squeeze(MeanValues_sorted(idxDrift,1:4,1,:))';

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
subplot(1,2,1);
    hold on;
    curData = squeeze(STSWD_summary.data_matched);
    idx = [idx_ya(lowChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'k', ...
        'linewidth', 2);
    % add high
    idx = [idx_ya(highChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--k', ...
        'linewidth', 2);
    idx = [idx_oa(lowChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'r', ...
        'linewidth', 2);
    % add high
    idx = [idx_oa(highChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylim([0 4]);
    xlabel('preferred feature');
    ylabel('Drift')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
subplot(1,2,2);
    hold on;
    curData = squeeze(STSWD_summary.data_feature);
    idx = [idx_ya(lowChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'k', ...
        'linewidth', 2);
    % add high
    idx = [idx_ya(highChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--k', ...
        'linewidth', 2);
    idx = [idx_oa(lowChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'r', ...
        'linewidth', 2);
    % add high
    idx = [idx_oa(highChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    ylim([0 4]);
    xlabel('feature');
    ylabel('Drift')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
if savefig == 1
    figureName = 'c1_mediansplit_yaoa_featurespecific';
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
end

%% calculate linear changes (w. outlier control)

% STSWD_summary.data_feature = squeeze(MeanValues(idxDrift,1:4,1:4,:));
% STSWD_summary.data_matched = squeeze(MeanValues_sorted(idxDrift,1:4,1:4,:));

X = [1 1; 1 2; 1 3; 1 4]; 
for indFeat = 1:4
    b=X\squeeze(MeanValues(idxDrift,indFeat,1:4,:)); 
    STSWD_summary.data_feature_linear(:,indFeat,1) = winsorize_outlier(b(2,:));
    
    b=X\squeeze(MeanValues_sorted(idxDrift,indFeat,1:4,:)); 
    STSWD_summary.data_matched_linear(:,indFeat,1) = winsorize_outlier(b(2,:));
end

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
subplot(1,2,1);
    hold on;
    curData = squeeze(STSWD_summary.data_matched_linear);
    idx = [idx_ya(lowChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'k', ...
        'linewidth', 2);
    % add high
    idx = [idx_ya(highChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--k', ...
        'linewidth', 2);
    idx = [idx_oa(lowChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'r', ...
        'linewidth', 2);
    % add high
    idx = [idx_oa(highChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    %ylim([0 4]);
    xlabel('preferred feature');
    ylabel('Drift (lin. mod.)')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
subplot(1,2,2);
    hold on;
    curData = squeeze(STSWD_summary.data_feature_linear);
    idx = [idx_ya(lowChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'k', ...
        'linewidth', 2);
    % add high
    idx = [idx_ya(highChIdxYA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--k', ...
        'linewidth', 2);
    idx = [idx_oa(lowChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, 'r', ...
        'linewidth', 2);
    % add high
    idx = [idx_oa(highChIdxOA)];
    error = squeeze(nanstd(curData(idx,:),[],1))./sqrt(numel(idx));
    errorbar(squeeze(nanmean(curData(idx,:),1)), error, '--r', ...
        'linewidth', 2);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1'; '2'; '3'; '4'}, ...
            'xlim', [0.5 4.5]);
    %ylim([0 4]);
    xlabel('feature');
    ylabel('Drift (lin. mod.)')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
if savefig == 1
    figureName = 'c1_mediansplit_yaoa_featurespecific_lin';
    saveas(h, fullfile(pn.plotFolder, figureName), 'epsc');
    saveas(h, fullfile(pn.plotFolder, figureName), 'png');
end